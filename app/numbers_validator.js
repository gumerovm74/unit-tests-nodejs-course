/**
 *
 * A simple class containing metods for val numbers
 * @class NumbersValidator
 */
class NumbersValidator {
  /**
   * Creates an instance of NumbersValidator.
   * @memberof NumbersValidator
   */
  constructor() {}

  /**
   *
   *
   * @param  {Number} number number too check
   * @return {Boolean} true if number even
   * @memberof NumbersValidator
   */
  isNumberEven(number) {
    const typeOfVariable = typeof number;
    if (typeOfVariable !== 'number') {
      throw new Error(`[${number}] is not of type 'Number it is ${typeOfVariable}`);
    } else {
      return number % 2 === 0;
    }
  }

  /**
   *
   *
   * @param {Array<Number>} arrayOfNumbers array of numbers to go through
   * @return {Array<Number} array of Even numbers
   * @memberof NumbersValidator
   */
  getEvenNumbersFromArray(arrayOfNumbers) {
    if (Array.isArray(arrayOfNumbers) &&
    arrayOfNumbers.every( (item) => typeof item === 'number')) {
      const arraOfEvennumbers = arrayOfNumbers.filter(this.isNumberEven);
      return arrayOfEvenNumbers;
    } else {
      throw new Error(`[${arrayOfNumbers}] is not an array of "Numbers"`);
    }
  }
};

module.exports = NumbersValidator;